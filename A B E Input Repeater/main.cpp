#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0500
#include <windows.h>

#define KEYDOWN(vkCode) ((GetKeyState(vkCode) & 0x8000) ? true : false)
#define KEYUP(vkCode) ((GetKeyState(vkCode) & 0x8000) ? false : true)

int main()
{
	bool run = true;
	ShowWindow(GetConsoleWindow(), SW_HIDE); // Hide program
	while(run)
	{
		if(KEYDOWN(0x43) && KEYDOWN(VK_CONTROL) && KEYDOWN(VK_MENU)) // CTRL + ALT + C
		{ // If Ctrl + Alt + C is pressed, stop the program
			run = false;
			break;
		}
		keybd_event(0x41, 0, 0, 0); // a
		keybd_event(0x42, 0, 0, 0); // b
		keybd_event(0x45, 0, 0, 0); // e
		keybd_event(0x0D, 0, 0, 0); // return
		
		// lift the keys
		keybd_event(0x0D, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(0x41, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(0x42, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(0x45, 0, KEYEVENTF_KEYUP, 0);
		Sleep(500);
	}
	 return 0;
}