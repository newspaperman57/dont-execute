#include <windows.h>

void CDRomOpen(BOOL bOpenDrive, char *drive)
{ 
    MCI_OPEN_PARMS open;
    DWORD flags;

    ZeroMemory(&open, sizeof(MCI_OPEN_PARMS));

    open.lpstrDeviceType = (LPCSTR) MCI_DEVTYPE_CD_AUDIO;
    open.lpstrElementName = drive;

    flags = MCI_OPEN_TYPE | MCI_OPEN_TYPE_ID;

    if (!mciSendCommand(0, MCI_OPEN, flags, (DWORD) &open)) 
    {
        mciSendCommand(open.wDeviceID, MCI_SET, (bOpenDrive) ? 
                       MCI_SET_DOOR_OPEN : MCI_SET_DOOR_CLOSED, 0);
        mciSendCommand(open.wDeviceID, MCI_CLOSE, MCI_WAIT, 0);
    }
}

int main()
{
	CDRomOpen(true, "E");
	return 0;
}